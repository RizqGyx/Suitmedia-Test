import React from 'react'
import Navbar from '../components/Navbar'
import Banner from '../components/Banner'
import ListPost from '../components/ListPost'

function Home() {
  return (
    <>
        <Navbar />
        <Banner />
        <ListPost />
    </>
  )
}

export default Home