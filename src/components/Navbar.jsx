import React, { useState, useEffect, useRef } from 'react'

const Navbar = () => {
    const [isOpen, setIsOpen] = useState(true);
    const [prevScrollPos, setPrevScrollPos] = useState(0);
    const [visible, setVisible] = useState(true);
    const [activeMenu, setActiveMenu] = useState('Ideas');

    useEffect(() => {
        const handleScroll = () => {
            const currentScrollPos = window.pageYOffset;
            setVisible(prevScrollPos > currentScrollPos || currentScrollPos < 20);
            setPrevScrollPos(currentScrollPos);
        };

        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    }, [prevScrollPos]);

    let dialogRef = useRef();

    let clickOutsideModal = (e) => {
        if (e.target.id !== 'dialog') {
            dialogRef.current.close();
            setIsOpen(!isOpen);
        }
    }

    const handleHamburger = () => {
        setIsOpen(!isOpen)
        dialogRef.current.showModal();
    }

    const shouldFixNavbar = window.pageYOffset > 0;

    const handleMenuClick = (menuName) => {
        setActiveMenu(menuName);
    };

    return (
        <nav className={`flex justify-between items-center w-full z-10 py-3 px-8 md:px-16 transition-all duration-300 ${shouldFixNavbar ? 'fixed top-0 left-0': ''} ${visible ? 'bg-[#eb6930] opacity-90' : 'opacity-0'}`}>
            <a href="/"><img src="/Logo.png" className='h-[70px]' alt="Logo Suitmedia" /></a>

            <div className='space-x-5 hidden md:block xl:text-lg text-white'>
                <a href="#work" className={`p-2 ${activeMenu === 'Work' ? 'border-b-4 border-white' : ''}`} onClick={() => handleMenuClick('Work')}>Work</a>
                <a href="#about" className={`p-2 ${activeMenu === 'About' ? 'border-b-4 border-white' : ''}`} onClick={() => handleMenuClick('About')}>About</a>
                <a href="#services" className={`p-2 ${activeMenu === 'Services' ? 'border-b-4 border-white' : ''}`} onClick={() => handleMenuClick('Services')}>Services</a>
                <a href="#ideas" className={`p-2 ${activeMenu === 'Ideas' ? 'border-b-4 border-white' : ''}`} onClick={() => handleMenuClick('Ideas')}>Ideas</a>
                <a href="#careers" className={`p-2 ${activeMenu === 'Careers' ? 'border-b-4 border-white' : ''}`} onClick={() => handleMenuClick('Careers')}>Careers</a>
                <a href="#contact" className={`p-2 ${activeMenu === 'Contact' ? 'border-b-4 border-white' : ''}`} onClick={() => handleMenuClick('Contact')}>Contact</a>
            </div>

            <dialog ref={dialogRef} onClick={clickOutsideModal}>
                <div id='dialog' className={`min-w[70vw] flex flex-col text-white/70 font-bold items-center fixed top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 rounded-lg bg-black/60 py-32 w-11/12 backdrop-blur-sm md:hidden`}>
                    <a href="#work" className='text-2xl my-3 hover:underline'>Work</a>
                    <a href="#about" className='text-2xl my-3 hover:underline'>About</a>
                    <a href="#services" className='text-2xl my-3 hover:underline'>Services</a>
                    <a href="#ideas" className='text-2xl my-3 hover:underline'>Ideas</a>
                    <a href="#careers" className='text-2xl my-3 hover:underline'>Careers</a>
                    <a href="#contact" className='text-2xl my-3 hover:underline'>Contact</a>
                </div>
            </dialog>

            <div className='flex md:hidden gap-6'>
                <button className='flex flex-col justify-center items-center md:hidden border p-2 rounded' onClick={handleHamburger}>
                    <span className={`bg-white block transition-all ease-out duration-300 h-0.5 w-6 rounded-sm py-0.5 ${!isOpen ? 'rotate-45 translate-y-2' : '-tranlate-y-1'}`}></span>
                    <span className={`bg-white block transition-all ease-in-out duration-300 h-0.5 w-6 rounded-sm py-0.5 my-1 ${!isOpen ? 'opacity-0 translate-x-8' : 'opacity-100'}`}></span>
                    <span className={`bg-white block transition-all ease-out duration-300 h-0.5 w-6 rounded-sm py-0.5 ${!isOpen ? '-rotate-45 -translate-y-2' : 'tranlate-y-1'}`}></span>
                </button>
            </div>
        </nav>
    )
}

export default Navbar