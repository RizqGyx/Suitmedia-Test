import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Card from './Card';
import { FaChevronLeft, FaChevronRight } from "react-icons/fa";

const ListPost = () => {
    const [posts, setPosts] = useState(() => {
        const storedPosts = localStorage.getItem('storedPosts');
        return storedPosts ? JSON.parse(storedPosts) : [];
    });
    const [currentPage, setCurrentPage] = useState(() => {
        const storedPage = localStorage.getItem('storedPage');
        return storedPage ? JSON.parse(storedPage) : 1;
    });
    const [postsPerPage, setPostsPerPage] = useState(() => {
        const storedPerPage = localStorage.getItem('storedPerPage');
        return storedPerPage ? JSON.parse(storedPerPage) : 10;
    });
    const [sortType, setSortType] = useState(() => {
        const storedSortType = localStorage.getItem('storedSortType');
        return storedSortType || 'published_at';
    });
    const [totalData, setTotalData] = useState(0);

    const formatDate = (dateString) => {
        const date = new Date(dateString.split(' ')[0]);
        const options = { year: 'numeric', month: 'long', day: 'numeric' };
        return date.toLocaleDateString('id-ID', options);
    };

    useEffect(() => {
        axios.get(`https://suitmedia-backend.suitdev.com/api/ideas`, {
            params: {
                'page[number]': currentPage,
                'page[size]': postsPerPage,
                append: ['small_image', 'medium_image'],
                sort: sortType,
            }
        })
            .then(response => {
                const formattedPosts = response.data.data.map(post => ({
                    ...post,
                    formattedDate: formatDate(post.published_at)
                }));
                const totalData = response.data.meta.total;
                setTotalData(totalData);
                setPosts(formattedPosts);
                console.log(response.data.data)
                localStorage.setItem('storedPosts', JSON.stringify(formattedPosts));
            })
            .catch(error => {
                console.error('Error fetching data:', error);
            });
    }, [currentPage, postsPerPage, sortType]);

    useEffect(() => {
        localStorage.setItem('storedPage', JSON.stringify(currentPage));
        localStorage.setItem('storedPerPage', JSON.stringify(postsPerPage));
        localStorage.setItem('storedSortType', sortType);
    }, [currentPage, postsPerPage, sortType]);

    const paginate = pageNumber => {
        setCurrentPage(pageNumber);
    };

    const changePostsPerPage = event => {
        setPostsPerPage(parseInt(event.target.value));
    };

    const changeSortType = event => {
        setSortType(event.target.value);
    };

    const startIndex = (currentPage - 1) * postsPerPage + 1;
    const endIndex = Math.min(currentPage * postsPerPage, totalData);
    const showingText = `Showing ${startIndex} - ${endIndex} of ${totalData}`;
    const maxButtonsToShow = 5;

    const renderPageButtons = () => {
        const totalPages = Math.ceil(totalData / postsPerPage);
        const currentButton = currentPage;
        const buttons = [];
    
        let startPage = Math.max(1, currentButton - Math.floor(maxButtonsToShow / 2));
        let endPage = Math.min(totalPages, startPage + maxButtonsToShow - 1);
    
        if (totalPages <= maxButtonsToShow) {
            startPage = 1;
            endPage = totalPages;
        } else if (endPage - startPage < maxButtonsToShow - 1) {
            startPage = endPage - maxButtonsToShow + 1;
        }
    
        for (let i = startPage; i <= endPage; i++) {
            buttons.push(
                <button
                    key={i}
                    onClick={() => paginate(i)}
                    className={`${currentPage === i ? 'bg-orange-500 text-white' : 'bg-white text-black'} rounded-md px-3 py-1`}
                >
                    {i}
                </button>
            );
        }
    
        return buttons;
    };

    return (
        <div className='w-11/12 mx-auto mt-10'>
            <div className='flex justify-between items-center mb-10'>
                <p>{showingText}</p>

                <div className='flex items-center gap-10'>
                    Show Per Page:
                    <select onChange={changePostsPerPage} value={postsPerPage}>
                        <option value={10}>10</option>
                        <option value={20}>20</option>
                        <option value={50}>50</option>
                    </select>

                    Sort by:
                    <select onChange={changeSortType} value={sortType}>
                        <option value="published_at">Newest</option>
                        <option value="-published_at">Oldest</option>
                    </select>

                </div>
            </div>

            <div className='grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-12'>
                {posts.map((post) => (
                    <Card
                        key={post.id}
                        imageURL={post.medium_image[0]?.url || 'https://via.placeholder.com/100'}
                        createdAt={post.formattedDate}
                        title={post.title}
                    />
                ))}
            </div>

            <div className='flex items-center w-11/12 mx-auto justify-center mt-10 gap-4'>
                <button onClick={() => paginate(currentPage - 1)} disabled={currentPage === 1}>
                    <FaChevronLeft />
                </button>

                {renderPageButtons()}

                <button onClick={() => paginate(currentPage + 1)} disabled={currentPage === Math.ceil(totalData / postsPerPage)}>
                    <FaChevronRight />
                </button>
            </div>

        </div>
    );
};

export default ListPost;