import React from 'react';

const Banner = () => {
  return (
    <div className="wrap-Hero mx-auto items-center bg-center lg:bg-fixed lg:bg-cover lg:justify-center">
      <div className="flex justify-center items-center h-[60vh] w-11/12 mx-auto">
        <div className="flex flex-col text-center text-white">
            <h1 className='text-5xl font-bold'>Ideas</h1>
            <p className='text-xl'>Where all our great things begin</p>
        </div>
      </div>
    </div>
  );
};

export default Banner;