import React from 'react'

function Card({imageURL, createdAt, title}) {
  return (
    <div className='h-75 max-w-[384px] overflow-hidden shadow-2xl cursor-pointer flex flex-col'>
        <img src={imageURL} alt={title} className='aspect-[17/9] w-full rounded object-cover' loading="lazy" />
        <div className='m-5'>
          <p className='text-gray-400'>{createdAt}</p>
          <h1 className='text-xl font-bold line-clamp-3'>{title}</h1>
        </div>
    </div>
  )
}

export default Card